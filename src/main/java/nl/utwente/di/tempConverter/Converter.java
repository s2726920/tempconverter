package nl.utwente.di.tempConverter;

public class Converter {

    public double changeTemp(String isbn) {
        return (Double.parseDouble(isbn) * 1.8) + 32.00;

    }

}